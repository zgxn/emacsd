(require 'package)
(add-to-list 'package-archives '("melpa-stable"
				 . "http://stable.melpa.org/packages/"))
(setq package-enable-at-startup nil)
(package-initialize)

;; package is now loaded, so we can check for other packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; ensure these are installed
(use-package ido
  :ensure t)
(use-package undo-tree
  :ensure t)
(use-package pdf-tools
  :ensure t)
(use-package magit
  :ensure t)
(use-package org
  :ensure t)
(use-package zenburn-theme
  :ensure t)

;; color theme
(require 'zenburn-theme)
(load-theme 'zenburn t)

;; activate stuff
(require 'ido)
(ido-mode t)

(require 'undo-tree)
(global-undo-tree-mode t)

(require 'pdf-tools)
(pdf-tools-install)
