;; editor

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(blink-cursor-mode 0)

(setq inhibit-splash-screen 1)
(setq inhibit-startup-message 1)

;; font size

(set-face-attribute 'default nil :height 150)

;; mode line configuration

(display-time-mode 1)
(display-battery-mode 1)

;; hooks and modes

(setq-default
 whitespace-line-column 80
 whitespace-style '(face lines-tail))
(global-whitespace-mode 1)

(add-hook 'prog-mode-hook 'linum-mode)
(add-hook 'prog-mode-hook 'show-paren-mode)

;; load files for behavior and package management

(load "~/.emacs.d/tweak/tweak.el")
(load "~/.emacs.d/package/package.el")

