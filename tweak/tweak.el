;; file behavior

(setq backup-directory-alist `(("." . "~/.emacs.d/saves")))

;; dired behavior

(put 'dired-find-alternate-file 'disabled nil)
